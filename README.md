# SimpleTextArchive

## What's this?

This is a small project creating a very simple text archive web application using Rust and PostgreSQL. The design is rather straightforward, with the HTTP component being simply a read-only interface to the database with all modifications executed using the program itself with appropriate parameters.

## Requirements

### Required
- PostgreSQL 12, with `btree_gist` extension and language-specific full text search resources available. Version 12 is required due to computed columns being used. If any other language than `english` is desired, source code modification is currently required. Database used should be UTF-8 - other encodings may result in complications due to the program assuming it is interacting with an UTF-8 database.
- Rust nightly (due to Rocket HTTP library requiring this).

### Optional

- Node 12.x and NPM (if web interface is to be extended or styles changed using the scaffold; not required for Rust or DB development)

## To install

1. Ensure you have appropriate dependencies available and installed
2. See the sample configuration file, and alter it to your needs where appropriate. In particular, ensure you have a database available and with appropriate privileges
3. Run `cargo run --release -- -c <configuration_file> initialize`. This will initialize your database with appropriate data structures.
4. Run `cargo run --release -- -c <configuration_file> serve`. This will launch the HTTP server, allowing you to browse.
5. Navigate to `/documents/SimpleTextArchive/README?operation=view`. If your installation completed successfully, this should reveal you a short README file.

## Usage

Most interactions with the database itself are handled via `cmd` and `bulk-cmd` commands. Both take subcommands in different forms, as described below

- `ingest <db_path> <description> <file>` reads a file from the local system, and ingests it to the database verbatim. This file should be UTF-8 for ingestion to be successful.

- `delete <db_path> ...` attemps to delete all paths given, reporting how many were deleted and which ones were not found

### cmd

`cmd` executes a simple command.

Example: `cargo run --release -- -c <configuration_file> cmd ingest "fiction/pride_and_prejudice.txt" "Pride and Prejudice by Jane Austen" "./pride_and_prejudice.txt"`

### bulk-cmd

`bulk-cmd` executes multiple commands from a file, separated by vertical bars.

Example: command `cargo run --release -- -c <configuration_file> bulk-cmd cmd.txt`, with `cmd.txt` containing

```
ingest|fiction/pride_and_prejudice.txt|Pride and Prejudice by Jane Austen|./pride_and_prejudice.txt
delete|SimpleTextArchive/README
```

## Licensing

All respective dependencies as mentioned in Cargo.toml and NPM dependencies are under their respective licenses, and belong to their authors.

This work itself is published under following MIT license:

```
Copyright 2020 Arttu Ylä-Sahra

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```