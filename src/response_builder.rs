///
/// SimpleTextArchive
/// 
/// response_builder.rs - Build HTTP responses for Rocket
/// 
/// Helper methods for interfacing between Rocket request interfaces and the database, building pages using templates

use std::io::Cursor;

use rocket::request::Request;
use rocket::response::{self, Response, Responder, Redirect};
use rocket::http::{ContentType, Status, RawStr};

use postgres::{Connection};
use rocket_contrib::templates::Template;

use serde::{Serialize};

/// Definition of a document query response. This can be either a direct raw text, a folder enumeration, or a redirect
pub enum DocumentQueryResponse {
    /// This request leads to a raw text. Value is a text file
    RawText(String),
    /// This leads to a folder enumeration. Value is a context with all necessary information
    FolderEnumeration(FolderListingContext),
    /// This is a redirect to some URL. URL is passed verbatim; be sure to verify the URL is valid before returning a redirect response!
    Redirect(String),
}

/// Folder listing context
#[derive(Serialize,Clone)]
pub struct FolderListingContext {
    // Query path, or document path
    pub query_path: String,
    pub items: Vec<FolderListingItem>
}

/// Folder listing item
#[derive(Serialize,Clone)]
pub struct FolderListingItem {
    /// Displayed name
    pub display_name: String,
    /// Path
    pub path: String,
    /// Type of item
    pub item_type: String,
    /// Description
    pub description: String
}

/// Query listing, containing all necessary information for showing a search query result page
#[derive(Serialize,Clone)]
pub struct QueryListing {
    /// What we queried?
    pub query: String,
    /// What page?
    pub page: i32,
    /// What's the URL for the next page?
    pub next_page_url: String,
    /// What's the URL for the previous page?
    pub prev_page_url: String,
    /// Should we show the next page link?
    pub should_show_next_page: bool,
    /// Query item list
    pub items: Vec<QueryListingItem>
}

/// A single item for query listing
#[derive(Serialize,Clone)]
pub struct QueryListingItem {
    /// Path to this item
    pub path: String,
    /// Description
    pub description: String,
    /// Highlight, pre-escaped
    pub highlight: String
}

/// Responder for a query listing, facilitating rendering
impl<'r> Responder<'r> for QueryListing {
    fn respond_to(self, req: &Request) -> response::Result<'r> {
        // Not much to do, consume ourself and pass on to templating engine
        Template::render("search_result", self).respond_to(req)
    }
}

/// Responder for document queries, determining the appropriate response type
impl<'r> Responder<'r> for DocumentQueryResponse {
    fn respond_to(self, req: &Request) -> response::Result<'r> {
        match self {
            DocumentQueryResponse::FolderEnumeration(list) => {
                // Forward to appropriate renderer
                Template::render("folder_listing", list).respond_to(req)
            }
            DocumentQueryResponse::RawText(input) => {
                // Raw text. Quite easy
                Response::build()
                .sized_body(Cursor::new(input))
                .header(ContentType::Plain)
                .ok()
            }
            DocumentQueryResponse::Redirect(doc_path) => {
                // Redirect
                Redirect::to("/documents/".to_owned() + &doc_path).respond_to(req)
            }
        }
    }
}

/// Build a search query response, effectively executing a database query and applying it to a proper template
pub fn build_search_query_response(db_conn: &Connection, query: String, page: i32) -> Result<QueryListing, Status> {
    // Coerce our page to minimum one
    let adjusted_page = if page < 1 {1} else {page};

    // Query
    let db_result = super::db_data_queries::do_search(db_conn, &query, page).map_err(|e| {error!("whilst requesting a search: {}", e); Status::InternalServerError})?;

    // Next, convert our results to items
    let items: Vec<QueryListingItem> = db_result.into_iter().map(|tuple| {
        // Extract all necessary elements
        let path = tuple.0;
        let description = tuple.1;
        // We need to convert and escape highlighting. Let's do it as follows
        let escaped_highlights = RawStr::from_str(&tuple.2).html_escape()
            .replace("$STA_BH_STA$", r#"<span class="search_highlight">"#)
            .replace("$STA_EH_STA$", r#"</span>"#)
            .replace("$STA_BR_STA$", r#"<b><br> ... <br></b>"#)
            .to_owned();

            QueryListingItem {path, description, highlight: escaped_highlights}
        
    }).collect();

    // Construct URLs

    // This does look pretty hazardous and I would STRONGLY prefer a better means.. but eh.. is there? Without brining in a whole new crate not totally needed?
    let next_page_url = format!("/search_query?text={}&page={}", rocket::http::uri::Uri::percent_encode(&query), adjusted_page+1);
    let prev_page_url = format!("/search_query?text={}&page={}", rocket::http::uri::Uri::percent_encode(&query), adjusted_page-1);
    let item_length = items.len();

    // Return a query listing
    Ok(QueryListing {items, next_page_url, prev_page_url, page: adjusted_page, should_show_next_page: item_length == 10, query})
}

/// Builds a document query response. This step basically selects the right query
/// Be sure to check that the path has been checked and safe for interpolation. The way we call it from main.rs, it should be safe as their PathBuf implementation checks for nasty URLs
///
/// We also know that our database _cannot_ return URLs that need further processing to escape them, as we have limited the scope of allowed URLs with a suitable regex. If this assumption is altered,
/// then we do also need to to more careful handling of our URLs as to ensure we cannot inject anything. One possible approach could be using URLencoded strings as keys - split by slashes as usual, but
/// make sure all intermediate parts are URLencoded. That way, we can still safely interpolate with more or less the same code, with more work required on the ingesting and DB end of things. Maybe future work for this?
pub fn build_document_query_response(db_conn: &Connection, path: String, req_type: Option<&str>) -> Result<DocumentQueryResponse, Status> {
    match req_type {
        Some("list") => {
            // Attempt to list directories
            let db_result = super::db_data_queries::get_directory_listing(db_conn, &path).map_err(|e| {error!("whilst requesting a directory listing: {}", e); Status::InternalServerError})?;

            if db_result.len() < 1 {
                // Empty directory listings are not interesting, so 404 for them. This also covers things like bad directory paths
                Err(Status::NotFound)
            } else {
                // Construct a context and return
                Ok(DocumentQueryResponse::FolderEnumeration(FolderListingContext{query_path: path, items: db_result.into_iter().map(|elem| {
                    FolderListingItem {
                        item_type: elem.0,
                        display_name: elem.1,
                        path: elem.2,
                        description: elem.3
                    }
                }).collect()}))
            }
        }
        Some("view") => {
            // Attempt to inquire from database
            let db_result = super::db_data_queries::get_document_full(db_conn, &path).map_err(|e| {error!("whilst requesting a document: {}", e); Status::InternalServerError})?;

            // Was this a valid document?
            if let Some(found_text) = db_result {
                Ok(DocumentQueryResponse::RawText(found_text))
            } else {
                Err(Status::NotFound)
            }
        },
        None => {
            // Attempt to determine if this exists as a document
            let was_found = super::db_data_queries::document_exists(db_conn, &path).map_err(|e| {error!("whilst inquiring for existence of a document: {}", e); Status::InternalServerError})?;

            // Should be safe, see note in function and our suffix is a known-safe query parameter
            if was_found {
                Ok(DocumentQueryResponse::Redirect(path + "?operation=view"))
            } else {
                Ok(DocumentQueryResponse::Redirect(path + "?operation=list"))
            }
        }
        _ => {
            Err(Status::BadRequest)
        }
    }
}