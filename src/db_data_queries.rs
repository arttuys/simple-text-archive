use postgres::{Connection, Result};

///
/// SimpleTextArchive
/// 
/// db_data_queries.rs - DB query procedures
/// 
/// Tools to query data from the database. None of these procedures alter database data in any way

/// Tries to get a document in full. Effectively, select all appropriate chunks in order and merge them
pub fn get_document_full(conn: &Connection, path: &str) -> Result<Option<String>> {
    let stmt = conn.prepare("SELECT string_agg(txt, '' ORDER BY chunk_seq ASC) FROM CHUNK WHERE document=$1;")?;

    let query_result = stmt.query(&[&path])?;

    // This should return at least one row, per aggregate function
    // Null may be returned for otherwise valid but just unsuccessful aggregates
    return Ok(query_result.get(0).get(0));
}

/// Check if a particular document exists. Returns true or false
pub fn document_exists(conn: &Connection, path: &str) -> Result<bool> {
    let stmt = conn.prepare("SELECT EXISTS(SELECT 1 FROM DOCUMENT WHERE document_path=$1);")?;

    let query_result = stmt.query(&[&path])?;

    return Ok(query_result.get(0).get(0));
}

/// Get a directory listing; this is indicated as a list of tuples containing type, display name, path and description (empty if not available)
/// Most of the logic is handled by the backend database function, but we need to merge the document description if available
pub fn get_directory_listing(conn: &Connection, path: &str) -> Result<Vec<(String, String, String, String)>> {
    let stmt = conn.prepare(r#"SELECT L.item_type,L.display_name,L.path,D.description FROM STA_LIST_DIRECTORY($1) L LEFT JOIN DOCUMENT D ON D.document_path=L.path ORDER BY display_name ASC;"#)?;

    let query_result = stmt.query(&[&path])?;

    // Map into appropriate structural parts
    return Ok(query_result.into_iter().map(|row| {
        let item_type: String = row.get(0);
        let display_name: String = row.get(1);
        let path: String = row.get(2);
        let description: Option<String> = row.get(3);

        (item_type, display_name, path, description.unwrap_or_default())
    }).collect());
}

/// Do a search; pagination is supported. Currently max results are 10 per page - a configuration option could perhaps be added for this?
pub fn do_search(conn: &Connection, query: &str, page: i32) -> Result<Vec<(String,String,String)>> {
    let stmt = conn.prepare(r#"SELECT * FROM STA_DO_SEARCH('english', $1, $2, 10);"#)?;

    // Handle bad page numbers; if page is under 1, coerce it to be 1. We need to subtract and multiply as well for the database OFFSET logic
    let adjusted_page = if page < 1 {0} else {(page-1)*10};

    let query_result = stmt.query(&[&query, &adjusted_page])?;

    return Ok(query_result.into_iter().map(|row| {
        let path: String = row.get(0);
        let description: String = row.get(1);
        let raw_highlights: String = row.get(2);

        (path, description, raw_highlights)
    }).collect())
}