#![feature(proc_macro_hygiene, decl_macro)]

///
/// SimpleTextArchive
/// 
/// main.rs - Main file
/// 
/// Entry point for the executable, and some basic logic for different actions, primarily data commands and serving HTTP

#[macro_use] extern crate rocket;
#[macro_use] extern crate log;
#[macro_use] extern crate rocket_contrib;

use postgres::{Connection, TlsMode};
use rocket_contrib::templates::Template;
use rocket_contrib::serve::StaticFiles;
use std::collections::HashMap;
use getopts::Options;
use rocket::config::{Config, Environment, Value};
use rocket::Request;

use rocket::http::Status;
use rocket::State;
use rocket::http::{RawStr};
use std::path::{PathBuf, Path};

use csv::ReaderBuilder;

mod db_data_queries;
mod config;
mod response_builder;
mod db_data_modification;

// Constants for options
static CONFIG_OPT: &str = "c";
static CONFIG_OPT_LONG: &str = "config";

static ALLOW_DEFAULT: &str = "d";
static ALLOW_DEFAULT_LONG: &str = "allow-default";

static POSTGRES_DB_IDENTIFIER: &str = "backend_postgres";

// Rocket database helper struct
#[database("backend_postgres")]
struct BackendDbConn(postgres::Connection);

//////////////////////////////////////////////////////////
/// Routes

// Document paths
// Observe from: https://api.rocket.rs/v0.4/rocket/request/trait.FromSegments.html
// --> As a result of these conditions, a PathBuf derived via FromSegments is safe to interpolate within, or use as a suffix of, a path without additional checks.
//
// As such, we can safely interpolate any path derived from here.
#[get("/documents/<path..>?<operation>")]
fn document_route(conn: BackendDbConn, path: PathBuf, operation: Option<&RawStr>) -> Result<response_builder::DocumentQueryResponse, Status> { // Note: declaring statuses directly is not encouraged according to Rocket manual, but for simplicity, it is done here as messages don't really vary
    response_builder::build_document_query_response(&*conn, path.to_string_lossy().to_string(), operation.map(|x| x.as_str()))
}

// Handle empty-path case
#[get("/documents?<operation>")]
fn document_empty(conn: BackendDbConn, operation: Option<&RawStr>) -> Result<response_builder::DocumentQueryResponse, Status> { // Note: declaring statuses directly is not encouraged according to Rocket manual, but for simplicity, it is done here as messages don't really vary
    response_builder::build_document_query_response(&*conn, "".to_owned(), operation.map(|x| x.as_str()))
}

// Index page
#[get("/")]
fn index(config: State<config::BaseConfiguration>) -> Template {
    // We need to provide a public identifier for the index page
    let mut context = HashMap::<String,String>::new();
    context.insert("public_identifier".to_owned(), config.public_identifier.clone());
    Template::render("index", context)
}

// Search form
#[get("/search")]
fn search_form() -> Template {
    // We need to provide a public identifier for the index page
    let context = HashMap::<String,String>::new();
    Template::render("search_form", context)
}

// Search query
#[get("/search_query?<text>&<page>")]
fn search_query(conn: BackendDbConn, text: String, page: Option<i32>) -> Result<response_builder::QueryListing, Status> {
    response_builder::build_search_query_response(&*conn, text, page.unwrap_or(1))
}

//////////////////////////////////////////////////////////
/// Error messages

#[catch(400)]
fn bad_request(_: &Request) -> String {
    format!("Sorry, this request was malformed. SimpleTextArchive was unable to interprete what you meant.")
}

#[catch(404)]
fn not_found(req: &Request) -> String {
    format!("Sorry, but nothing was found from '{}'. Please check that you have entered the address correctly, and try again.", req.uri())
}

#[catch(500)]
fn internal_error() -> String {
    format!("Something went wrong with your request. This may be a transient fault - if it persists, please contact the site administrator.")
}

//////////////////////////////////////////////////////////
/// General scaffolding

/// Construct getopts options
fn construct_options() -> Options {
    let mut opts = Options::new();
    opts.optopt(CONFIG_OPT, CONFIG_OPT_LONG, "specify configuration file to use", "FILENAME");
    opts.optflag(ALLOW_DEFAULT, ALLOW_DEFAULT_LONG, "allow default options to be written to a file and used");
    return opts;
}

/// Print instructions for the program file itself
fn print_instructions(program: &str, opts: Options) {
    let brief = format!("To use: {} serve/ingest [options]", program);
    print!("{}", opts.usage(&brief));
}

/// Entry point. Determine appropriate steps to take, and do necessary initialization procedures
fn main() -> Result<(), String> {
    env_logger::init();
    info!("Starting simple-text-archive...");

    // First, parse options
    let opts = construct_options();
    let args: Vec<String> = std::env::args().collect();
    let program = args[0].clone();
    let matches = match opts.parse(&args[1..]) {
        Ok(args) => {args}
        Err(e) => {panic!(e.to_string())}
    };

    // Set up configuration loading options
    let allow_default: bool = matches.opt_present(ALLOW_DEFAULT);
    let config_path = matches.opt_str(CONFIG_OPT);

    // Attempt to load base configuration
    let base_configuration = config::load_config(config_path.as_deref(), allow_default)?;

    info!("Loaded configuration: {}", base_configuration);

    // Build a mapped database URL; if our URL is set to "ENV", then utilize the environment variable instead
    let mapped_db_url = if &base_configuration.database_url == "ENV" {info!("DB url is ENV, using environment variable 'TEXTARCHIVE_DB'"); std::env::var("TEXTARCHIVE_DB").map_err(|err| format!("whilst inspecting TEXTARCHIVE_DB environment variable: {}", err))?} else {base_configuration.database_url.clone()};

    // Check if we have a command
    if matches.free.is_empty() {
        print_instructions(&program, opts);
        return Ok(());
    } else {
        let cmd = matches.free[0].as_ref();

        match cmd {
            // Execute a bulk command set - load commands from file, and execute in order
            "bulk-cmd" => {
                let file: String = matches.free.get(1).ok_or("input file required for bulk commands".to_owned())?.to_string();

                let conn = Connection::connect(mapped_db_url, TlsMode::None).map_err(|err| format!("whilst connecting to DB to execute bulk commands: {}", err))?;

                // Build a bulk reader
                let mut csv_reader = ReaderBuilder::new().delimiter(b'|').has_headers(false).flexible(true).from_path(file).map_err(|err| format!("whilst building or running the bulk command reader: {}", err))?;

                info!("Collecting and reading bulk commands, this may take a while...");
                let mut bulk_command_vec: Vec<Vec<String>> = vec![];

                // Collect into a bulk command vector. This could, perhaps, possibly, be turned into a dynamic iterator? This also ensures though that expensive DB transactions are not ran until at least the file is confirmed to be in an apparently valid (but possibly semantically bad) format
                for result_record in csv_reader.records() {
                    let record = result_record.map_err(|err| format!("whilst parsing a row: {}", err))?;
                    bulk_command_vec.push(record.into_iter().map(|x| x.to_owned()).collect());
                }

                // Execute
                db_data_modification::execute_user_commands(&conn, bulk_command_vec)?;

                info!("Done");
            }
            // Execute a single command
            "cmd" => {
                let command: Vec<String> = matches.free.get(1..).ok_or("at least one argument needed for data commands. Valid commands: ingest".to_owned())?.to_vec();

                let conn = Connection::connect(mapped_db_url, TlsMode::None).map_err(|err| format!("whilst connecting to DB to execute a command: {}", err))?;

                info!("Executing a single command");

                // Build a single command structure to execute
                db_data_modification::execute_user_commands(&conn, &[command])?;

                info!("Done");
            }
            // Initialize the database to an empty state
            "initialize" => {
                let conn = Connection::connect(mapped_db_url, TlsMode::None).map_err(|err| format!("whilst connecting to DB to initialize: {}", err))?;

                info!("Initializing DB to empty defaults");
                db_data_modification::initialize_db(&conn).map_err(|err| format!("whilst running initialization script: {}", err))?;
                info!("OK, initialized to empty defaults");
            }
            // Serve over HTTP
            "serve" => {

                // Take a clone from our config; we'll use it later
                let managed_base_config = base_configuration.clone();

                // Initialize Rocket DB framework
                let mut databases = HashMap::new();
                let mut postgres_config = HashMap::new();
            
                postgres_config.insert("url", Value::from(mapped_db_url));
                databases.insert(POSTGRES_DB_IDENTIFIER, Value::from(postgres_config));

                // Configure Rocket
                let rocket_config = Config::build(if cfg!(debug_assertions) {Environment::Development} else {Environment::Production})
                                    .address(base_configuration.bind_ip)
                                    .port(base_configuration.bind_port)
                                    .extra("databases", databases)
                                    .root({
                                        if let Some(http_path) = base_configuration.http_template_path {
                                            let conf_file_dir = Path::new(&http_path).to_path_buf().canonicalize().expect("unable to canonicalize HTTP template directory");
                                            info!("Using '{}' as HTTP base path", conf_file_dir.to_string_lossy());
                                            conf_file_dir
                                        } else if let Some(config_file_path) = base_configuration.config_file_path {
                                            let conf_file_dir = config_file_path.parent().expect("no parent directory available for config file..?").canonicalize().expect("unable to canonicalize config directory");
                                            info!("No separate HTTP base path provided, using configuration file location: {}", conf_file_dir.to_string_lossy());
                                            conf_file_dir.to_path_buf()
                                        } else {
                                            panic!("Could not deterministically determine an appropriate HTTP base path!")
                                        }
                                    })
                                    .finalize().map_err(|err| format!("whilst launching HTTP framework: {}", err))?;

                // Let's validate that our HTTP template directory is OK, in the sense that "templates" and "static" exist
                let determined_root = rocket_config.root().expect("no valid root directory found from Rocket configuration");

                if determined_root.is_dir() {
                    // Check these subdirectories
                    let subdirs = vec!["static", "templates"];

                    for subdir in subdirs {
                        let path = determined_root.join(subdir);
                        if !path.is_dir() {
                            error!("Directory '{}' was not found from the HTTP base path. This is required - are you sure the path is correct?", path.to_string_lossy());
                            panic!("Cannot continue, invalid HTTP base path configuration");
                        }
                    }
                } else {
                    error!("HTTP template path '{}' does not point into a valid directory. Are you sure it is correct?", determined_root.to_string_lossy());
                    panic!("Cannot continue, invalid HTTP base path configuration");
                }
                // Constuct a static path
                let static_path = determined_root.join("static");

                // Everything checks out? Good, let's proceed in attaching all necessary Rocket parts and launching it
                let rocket_app = rocket::custom(rocket_config);
                rocket_app
                    .attach(BackendDbConn::fairing())
                    .attach(Template::fairing())
                    .mount("/", routes![index, document_route, document_empty, search_form, search_query])
                    .mount("/static", StaticFiles::from(static_path))
                    .manage(managed_base_config)
                    .register(catchers![not_found, internal_error, bad_request]).launch();
            }
            // Not a valid command? Stop here
            _ => {
                error!("Invalid command '{}'", cmd);
                return Err("no valid command was given".to_owned());
            }
        }
    }


    Ok(())
}