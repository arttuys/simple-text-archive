use serde::{Serialize,Deserialize};
use std::path::PathBuf;
use std::fmt;

///
/// SimpleTextArchive
/// 
/// configuration.rs - Configuration structures
/// 
/// Baseline program-wide configuration structures, and means to load configuration from a file

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(deny_unknown_fields)]
/// Baseline configuration
pub struct BaseConfiguration {
    /// What IP to bind to
    pub bind_ip: String,
    /// What port to bind to
    pub bind_port: u16,

    /// DB connection URL
    pub database_url: String,
    /// What to use as a Rocket root path, including HTTP templates and static stuff
    pub http_template_path: Option<String>,

    /// What is our identifier as shown?
    #[serde(default)]
    pub public_identifier: String,
    /// Passthrough variable for configuration file path - for apparent reasons, this should neither be serialized or deserialized
    #[serde(skip)]
    pub config_file_path: Option<PathBuf>
}

/// Default configuration - in case of user choice, provide reasonable defaults
impl Default for BaseConfiguration {
    fn default() -> Self {
        BaseConfiguration {
            bind_ip: "localhost".to_owned(),
            bind_port: 8080,

            database_url: "postgres://textarchive@localhost/textarchive".to_owned(),
            config_file_path: None,
            http_template_path: Some("./http_root".to_owned()),
            public_identifier: "".to_owned(),
        }
    }
}

/// Provide a simple text conversion implementation
impl fmt::Display for BaseConfiguration {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<identifier: {}> bound to {}:{}, using database URL {}", self.public_identifier, self.bind_ip, self.bind_port, self.database_url)
    }
} 

/// Attempt to load a config file. Optionally, a path may be provided to look in - otherwise, try default path in relation to executable
/// Results either in a valid configuration or an error message
pub fn load_config(path: Option<&str>, allow_default_overwrite: bool) -> Result<BaseConfiguration, String> {
    // Determine the actual load path according to provided options
    let actual_load_path: PathBuf = path.map(|path_str| {let mut pathbuf: PathBuf = PathBuf::new(); pathbuf.push(path_str); pathbuf}).unwrap_or_else(|| {
        warn!("No valid config path provided, using default path in relation to executable");
        let mut pathbuf = std::env::current_exe().map(|exe| exe.parent().expect("path invalid - no root path for executable file?").to_path_buf()).expect("unable to determine the location of the executable, and no other config path was provided");

        pathbuf.push("config.toml");
        return pathbuf;
    });

    info!("Reading configuration from '{}' ...", actual_load_path.to_string_lossy());

    // Determine what steps we should do
    if actual_load_path.is_file() {
        // Load and return
        let toml_file = std::fs::read_to_string(&actual_load_path).map_err(|err| format!("whilst reading config file: {}", err))?;
        let mut parsed_config: BaseConfiguration = toml::from_str(&toml_file).map_err(|err| format!("whilst parsing configuration: {}", err))?;

        // Append configuration path
        parsed_config.config_file_path = Some(actual_load_path.clone());

        return Ok(parsed_config);
    } else {
        // If our load path exists but it could not be recognized as a file..
        if actual_load_path.exists() {
            error!("Configuration file doesn't seem to be a recognizable file or is inaccessible. Exiting..");
            return Err("configuration path is not a valid file".to_owned());
        } else {
            // Do we allow default config to be written
            if allow_default_overwrite {
                // If so, then write default config as such
                warn!("No valid configuration file exists, using default configuration and writing it to a file");
                let mut default_config: BaseConfiguration = BaseConfiguration::default();
                let serialized_config = toml::to_string(&default_config).map_err(|err| format!("whilst serializing default configuration: {}", err))?;

                info!("Writing default configuration to: '{}'", actual_load_path.to_string_lossy());
                std::fs::write(&actual_load_path, serialized_config).map_err(|err| format!("whilst writing default configuration: {}", err))?;

                info!("Wrote default configuration successfully");

                default_config.config_file_path = Some(actual_load_path.clone());
                Ok(default_config)
            } else {
                // No? Error out
                return Err("no valid configuration available and creating a default config file was inhibited".to_owned())
            }
        }
    }
}