use postgres::{Connection};

///
/// SimpleTextArchive
/// 
/// db_data_modification.rs - DB modification procedures
/// 
/// Tools to change data in the database. Primarily, this is via execute_user_commands(), which takes command sequences

/// Import initialization script to a string
static INITIALIZATION_SCRIPT: &str = include_str!("./initialization.pgsql");

/// Attempts to initialize the database to specifications required by simple-text-archive
pub fn initialize_db(conn: &Connection) -> postgres::Result<()> {
    let transaction = conn.transaction()?;

    transaction.batch_execute(INITIALIZATION_SCRIPT)?;

    transaction.commit()?;

    Ok(())
}

/// Execute user commands. This takes an iterator which contains iterators which contain something that can be translated to strings
/// The typing could perhaps be improved with AsRef, but the type checker ended up in such a twist that I decided for a somewhat easier, yet admittedly less memory-friendly alternative
/// 
/// Currently, it is also assumed that the language is English. Further work should extend this
pub fn execute_user_commands<'a, I, J, S>(db: &Connection, commands: I) -> Result<(), String>
where
    S: ToString + 'a,
    J: IntoIterator<Item=S>,
    I: IntoIterator<Item=J>,
{
    // Initiate transaction
    let transaction = db.transaction().map_err(|e| format!("whilst starting a transaction: {}", e))?;

    // Iterate through commands
    for command_subiter in commands {
        // Extract the entire command to a vector
        let command_vec: Vec<String> = command_subiter.into_iter().map(|x| x.to_string()).collect();

        if command_vec.len() < 1 {
            return Err("command slice was empty!".to_owned());
        }

        // Extract the first part, which declares the operation
        let command: &str = &command_vec[0];

        // Match to appropriate code
        match command {
            "ingest" => {
                // Take a subslice
                let params = command_vec.get(1..).ok_or("more arguments needed".to_owned())?;
                if params.len() != 3 {
                    return Err("ingestion takes exactly 3 parameters: path, description and file name for string input".to_owned());
                }

                // Read file
                let ingest_file = std::fs::read_to_string(&params[2]).map_err(|e| format!("whilst reading a file for '{}', from {}: {}", params[0], params[2], e))?;

                // Perform
                &transaction.query("SELECT STA_MAKE_DOCUMENT('english', $1,$2,$3);", &[&params[0], &params[1], &ingest_file]).map_err(|e| format!("whilst executing query adding a document: {}", e))?;

                info!("Added document {} to database", params[0]);
            }
            "delete" => {
                let params = command_vec.get(1..).ok_or("more paths needed for removal".to_owned())?;
                let mut deleted_docs: i64 = 0;
                for path in params {
                    // See: https://stackoverflow.com/a/22546994
                   let deletion_result: i64 = transaction.query(r#"WITH deleted AS (DELETE FROM DOCUMENT WHERE document_path=$1 RETURNING *) SELECT COUNT(*) FROM deleted;"#, &[&path]).map_err(|e| format!("whilst executing query deleting a document: {}", e))?.get(0).get(0);
                   if deletion_result < 1 {
                     warn!("Path {} could not be found, ignored", path);
                   }
                   deleted_docs = deleted_docs.saturating_add(deletion_result);
                }

                info!("Deleted {}/{} paths in database from given paths", deleted_docs,params.len());
            }
            _ => return Err(format!("unknown command: {}", command))
        };
    }

    info!("All commands successfully executed");

    transaction.set_commit();

    Ok(())
}