-- This extension is required
-- Also: we assume that the encoding of our DB is UTF8; however, this is not automatically checked
CREATE EXTENSION IF NOT EXISTS btree_gist;

-- Drop existing tables
DROP TABLE IF EXISTS CHUNK,DOCUMENT CASCADE;

-- Drop existing types
DROP TYPE IF EXISTS STA_DIRECTORY_LISTING CASCADE;

-- Check for valid input patterns
DROP FUNCTION IF EXISTS STA_CHECK_VALID_DIR_PATTERN CASCADE;
CREATE OR REPLACE FUNCTION STA_CHECK_VALID_DIR_PATTERN(input text, allow_empty boolean) RETURNS boolean
    AS $$
        BEGIN
            IF input='' AND allow_empty THEN
                RETURN TRUE;
            END IF;
            -- Careful! This regex is crucial - this enforces that we don't have to handle particularly nasty URLs
            RETURN (input ~ '\A[a-zA-Z0-9\.\-\_]+(\/[a-zA-Z0-9\.\-\_]+)*\Z') AND NOT (input LIKE '%/..');
        END;
    $$
LANGUAGE PLPGSQL IMMUTABLE;

-- Calculate a path level. A path level is defined as count of slashes; 0 means there exist none, 1 there is at least one slash, etc..
-- With the above regex definition, it effectively translates to the subdirectory level, with 0 being the root level, 1 being one subdirectory deep, and so forth..
DROP FUNCTION IF EXISTS GET_PATH_LEVEL CASCADE;
CREATE OR REPLACE FUNCTION GET_PATH_LEVEL(query_path text) RETURNS INTEGER
    AS $$
        BEGIN
            RETURN length(query_path) - length(replace(query_path, '/', ''));
        END;
    $$
LANGUAGE PLPGSQL IMMUTABLE;

-- Documents are stored as a flat structure, with a pseudo-hierachical path. This comes with following benefits:
--  - Get operations are O(1) with an exact path
--  - Moving documents doesn't take structural traversal, but only a simple path change. Folders also pass in and out of existence automatically where needed.
-- but also with following disadvantages
--  - Folder listing needs to scan a fair bit of the database. Path levels help a bit by allowing quick determination of possible candidates; however, this may not help much in an unbalanced (one path level has way more than others) database. At worst, it could appear as an interesting performance pattern.
--    This can perhaps be mitigated to a degree with a suitable index.
--
--  - Empty folders cannot exist. There is no way to describe a directory with no contents, considering folders are effectively a byproduct of document definitions
CREATE TABLE DOCUMENT (
    document_path TEXT PRIMARY KEY CHECK (STA_CHECK_VALID_DIR_PATTERN(document_path,false)), -- Require that all paths are more or less well formed
    description TEXT NOT NULL,
    path_level NUMERIC GENERATED ALWAYS AS (GET_PATH_LEVEL(document_path)) STORED
);

-- Create an index allowing searching by path level and with ordering; this can optimize a bit
CREATE INDEX document_path_level_idx ON DOCUMENT(document_path, path_level ASC);

-- Create a document chunk
CREATE TABLE CHUNK (
    document TEXT NOT NULL REFERENCES DOCUMENT(document_path) ON DELETE CASCADE,
    txt TEXT NOT NULL,
    ts_lang regconfig NOT NULL,
    chunk_seq INTEGER NOT NULL, -- Chunk sequence number. This indicates the order in which chunks are to be read
    chunk_start INTEGER NOT NULL, -- Chunk start index by character. In theory, it would be possible to have chunks in non-increasing order, but it is not implemented in any fashion at this time.
    tsvector tsvector GENERATED ALWAYS AS (to_tsvector(ts_lang, txt)) STORED,
    chunk_range int4range GENERATED ALWAYS AS (int4range(chunk_start, (chunk_start + length(txt)-1), '[]')) STORED,
    CHECK (length(txt) > 0), -- Chunk texts must be nonempty
    CHECK (octet_length(convert_to(txt, 'UTF8')) <= 500000), -- One chunk cannot be larger than slightly under half a megabyte in UTF-8. This does not guarantee suitability for a TSVector, but limits the scale of chunks.
    CHECK (chunk_seq >= 0), -- Chunk sequence number start from zero
    CHECK (chunk_start >= 0), -- Chunks start from zero
    CHECK (txt NOT LIKE '%$STA\___\_STA$%'), -- Text cannot contain special escape symbols
    EXCLUDE USING gist (chunk_range WITH &&, document WITH =), -- Ranges should not overlap in the same document. This will not prevent disjoint ranges tho; this has to be handled at software level if necessary
    PRIMARY KEY(document, chunk_seq)
);

-- Create an additional index solely on documents and ascending chunk sequences
CREATE UNIQUE INDEX chunks_for_document_idx ON CHUNK(document, chunk_seq ASC);

------------- Other general functions

-- Ideally, there'd be a DB-endorsed way to safely escape user inputs from such inputs
-- As none such exists, we have to make our own. Luckily, this is a relatively
-- simple task as LIKE patterns are very simple
DROP FUNCTION IF EXISTS ESCAPE_LIKE_PATTERN CASCADE;
CREATE OR REPLACE FUNCTION ESCAPE_LIKE_PATTERN(input text) RETURNS text
    AS $$
        BEGIN
            RETURN replace(replace(replace(input, '\', '\\'), '%', '\%'), '_', '\_');
        END;
    $$
LANGUAGE PLPGSQL IMMUTABLE;

-- Attempts to find a safe cutting point for a chunk. Returns a safe final character offset. 
-- Safe final characters are primarily a newline or a space, secondarily any ASCII character. If none exists, returns -1
-- This is to avoid cutting at an UTF-8 multi-byte character, which could result in malformed files with misfortune. It is also to avoid cutting mid-word where possible
DROP FUNCTION IF EXISTS STA_FIND_SAFE_CUT CASCADE;
CREATE OR REPLACE FUNCTION STA_FIND_SAFE_CUT(data bytea) RETURNS integer
    AS $$
        DECLARE
            ascii_found boolean := false;
            index integer := octet_length(data) - 1;
            default_cut integer := index;
            current_byte integer := 0;
        BEGIN
            LOOP
                -- If index is below zero, return what we've found
                IF index < 0 THEN
                    RETURN default_cut;
                END IF;

                current_byte := get_byte(data, index);

                IF current_byte = 32 OR current_byte = 10 THEN
                    -- Space or line feed are safe stopping points, cut there.
                    RETURN index;
                END IF;

                IF ascii_found = false AND current_byte < 128 THEN
                    -- This should be safe, but look onwards still for a better, more desirable cut.
                    ascii_found := true;
                    default_cut := index;
                END IF;

                index := index - 1;
            END LOOP;
        END;
    $$
LANGUAGE PLPGSQL IMMUTABLE;

-- Ingest a text snippet to a new document. This handles chunking and creating necessary data structures, hiding the complexity from the downstream application level.
DROP FUNCTION IF EXISTS STA_MAKE_DOCUMENT CASCADE;
CREATE OR REPLACE FUNCTION STA_MAKE_DOCUMENT(lang regconfig, document_path text, description text, content text) RETURNS text
    AS $$
        DECLARE
            content_as_bytea bytea := convert_to(content, 'UTF8'); -- Convert text to UTF8 bytes
            max_chunk_length integer := 500000; -- How large one chunk can be in bytes
            byte_length integer := octet_length(content_as_bytea); -- How large the text is
            current_cursor_pos integer := 0; -- Current cursor position, for chunking
            safe_chunking_point integer; -- What safe chunking point has been discovered
            current_chunk_seq integer := 0; -- At what chunk sequence number we are currently at?
        BEGIN
            -- Begin by inserting a document record.
            INSERT INTO DOCUMENT VALUES (document_path, description);
            -- Then start creating chunks.
            -- Short circuit case; if length is less than one, exit now
            LOOP
                IF current_cursor_pos>=byte_length THEN
                    -- We have ran out of bytes to chunk or somehow else ran over boundary. Return document path.
                    RAISE NOTICE 'Ran out of bytes to chunk, exiting';
                    RETURN document_path;
                END IF;
                IF byte_length-current_cursor_pos <= max_chunk_length THEN
                    -- This fits into a chunk without cutting. Do so and return.
                    INSERT INTO CHUNK VALUES (document_path, convert_from(substring(content_as_bytea from (current_cursor_pos+1)), 'UTF-8'),lang, current_chunk_seq, current_cursor_pos);
                    RETURN document_path;
                END IF;
                -- Let's find a safe chunking point
                safe_chunking_point := STA_FIND_SAFE_CUT(substring(content_as_bytea from (current_cursor_pos+1) for max_chunk_length))+current_cursor_pos;
                -- Insert into chunks
                INSERT INTO CHUNK VALUES (document_path, convert_from(substring(content_as_bytea from (current_cursor_pos+1) for (safe_chunking_point-current_cursor_pos+1)), 'UTF8'),lang, current_chunk_seq, current_cursor_pos);
                current_cursor_pos := safe_chunking_point + 1;
                current_chunk_seq := current_chunk_seq + 1;
            END LOOP;
            -- Normally, we'd return a document path in the loop. In the unexpected event that we somehow manage to exit the loop here, raise an exception
            RAISE EXCEPTION 'Somehow escaped document insertion loop, this should not have happened and it is a bug';
        END;
    $$
LANGUAGE PLPGSQL;

------------- Directory listing

-- Create a directory listing type
CREATE TYPE STA_DIRECTORY_LISTING AS (item_type text, display_name text, path text);

-- Split a path at path level, resulting in a prefix and suffix
DROP FUNCTION IF EXISTS SPLIT_FROM_PATH_LEVEL CASCADE;
CREATE OR REPLACE FUNCTION SPLIT_FROM_PATH_LEVEL(query_path text, level integer, out prefix text, out suffix text) RETURNS RECORD
    AS $$
    DECLARE
        remaining_path_level integer := level;
        slashes_found boolean := true;
        suffix_as_array text[];
    BEGIN
        -- Initialize by putting the entire string into suffix
        suffix := query_path;
        -- And nothing into prefix
        prefix := '';

        IF suffix='' THEN
            -- We cannot do much to empty strings. Handle them gracefully
            RETURN;
        END IF;

        WHILE ((remaining_path_level > 0) AND slashes_found) LOOP
            slashes_found := false;
            suffix_as_array := string_to_array(suffix, NULL);

            --RAISE NOTICE 'Current prefix : %', prefix;
            --RAISE NOTICE 'Current suffix: %', suffix;

            FOR x IN 0..(array_length(suffix_as_array, 1)-1) LOOP
                DECLARE
                    found_positions integer[];
                    split_index integer;
                BEGIN
                    found_positions := array_positions(suffix_as_array, '/');
                    IF array_length(found_positions, 1) > 0 THEN
                        slashes_found := true;
                        split_index := found_positions[1]; -- OBS: 1-based

                        --RAISE NOTICE 'Split index: %', split_index;

                        IF prefix != '' THEN
                            -- If nonempty, apply a slash
                            prefix := prefix || '/';
                        END IF;

                        prefix := (prefix || substr(suffix,1,(split_index-1)));
                        suffix := substr(suffix, (split_index+1));
                        -- Exit from this loop, decrementing our path level
                        remaining_path_level := remaining_path_level - 1;
                        EXIT;
                    ELSE
                        -- Nothing else needed, just exit
                        EXIT; 
                    END IF;
                END;
            END LOOP;
        END LOOP;

        -- If we ran out of levels, append to empty
        IF remaining_path_level > 0 THEN
            IF prefix != '' THEN
                prefix := prefix || '/' || suffix;
            ELSE
                prefix := suffix;
            END IF;

            suffix := '';
        END IF;
    END;
    $$
LANGUAGE PLPGSQL IMMUTABLE;

-- Get last component in a path
DROP FUNCTION IF EXISTS GET_LAST_PATH_COMPONENT CASCADE;
CREATE OR REPLACE FUNCTION GET_LAST_PATH_COMPONENT(path text) RETURNS TEXT
    AS $$
        BEGIN
            RETURN (SPLIT_FROM_PATH_LEVEL(path, GET_PATH_LEVEL(path))).suffix;
        END; 
    $$
LANGUAGE PLPGSQL IMMUTABLE;

-- Get first component in a path
DROP FUNCTION IF EXISTS GET_FIRST_PATH_COMPONENT CASCADE;
CREATE OR REPLACE FUNCTION GET_FIRST_PATH_COMPONENT(path text) RETURNS TEXT
    AS $$
        BEGIN
            RETURN (SPLIT_FROM_PATH_LEVEL(path, 1)).prefix;
        END; 
    $$
LANGUAGE PLPGSQL IMMUTABLE;

DROP FUNCTION IF EXISTS BUILD_RELATIVE_PATH CASCADE;
-- Build a relative path for folder purposes. This essentially strips all but the immediate next component out.
CREATE OR REPLACE FUNCTION BUILD_RELATIVE_PATH(doc_path text, file_list_level integer) RETURNS TEXT
    AS $$
        BEGIN
            -- First, take appropriate suffix of the provided path. Take first component of said path
            RETURN GET_FIRST_PATH_COMPONENT((SPLIT_FROM_PATH_LEVEL(doc_path, file_list_level)).suffix);
        END;
    $$ 
LANGUAGE PLPGSQL IMMUTABLE;

-- List a directory. This lists both files and subfolders, by providing a path and explicit links
DROP FUNCTION IF EXISTS STA_LIST_DIRECTORY CASCADE;
CREATE OR REPLACE FUNCTION STA_LIST_DIRECTORY(query_path text) RETURNS SETOF STA_DIRECTORY_LISTING
    AS $$
        DECLARE
            path_joiner text := '/';
            level_offset integer := 0;
            file_path_level integer;
        BEGIN
            IF (STA_CHECK_VALID_DIR_PATTERN(query_path,true) <> true) THEN
                -- Require that all given directory patterns are valid. If not so, return an empty list.
                RAISE NOTICE 'Bad directory form %, not returning anything', query_path;
                RETURN;
            END IF;

            -- Special case; if we have an empty query, adjust accordingly
            IF query_path='' THEN
                path_joiner := '';
                level_offset := -1;
            END IF;

            -- Calculate appropriate file path level
            file_path_level := (GET_PATH_LEVEL(query_path)+1+level_offset);

            -- First, list all appropriate files. This is defined as starting with the desired query path, and landing on appropriate path level.
            RETURN QUERY SELECT 'file' as item_type, PL.suffix as display_name, D.document_path as path FROM DOCUMENT D LEFT JOIN LATERAL SPLIT_FROM_PATH_LEVEL(D.document_path, file_path_level) PL ON true WHERE (document_path LIKE (ESCAPE_LIKE_PATTERN(query_path) || (path_joiner) || ('%'))) AND path_level=file_path_level;

            -- Then, the next tricky step. We need to list possible directories.
            -- This is measured by all distinct paths, as measured by being:
            --  1) Path level being more than our current level.
            --  2) Next path component being distinct. This would otherwise return duplicate results
            RETURN QUERY SELECT DISTINCT 'folder' as item_type, RP as display_name, (query_path || path_joiner || RP) as path FROM DOCUMENT D LEFT JOIN LATERAL BUILD_RELATIVE_PATH(D.document_path,file_path_level) RP ON true WHERE (document_path LIKE (ESCAPE_LIKE_PATTERN(query_path) || (path_joiner) || ('%'))) AND path_level>file_path_level;
        END;
    $$
LANGUAGE PLPGSQL;

DROP FUNCTION IF EXISTS STA_DO_SEARCH CASCADE;
-- Do a search. This interpretes a query and generates appropriate headlines for the full text results
CREATE OR REPLACE FUNCTION STA_DO_SEARCH(lang regconfig, websearch_query text, query_offset integer, query_limit integer)
    RETURNS TABLE(document_path text, document_description text, document_raw_highlight text)
    AS $$
        DECLARE
            generated_query tsquery := websearch_to_tsquery(lang,websearch_query);
            query_weights float4[] := '{0.1, 0.2, 0.4, 1.0}';
            query_normalization integer := 16; -- Divide rank by 1 + logarithm of unique words in document.
            prelim_result record;
        BEGIN
            FOR prelim_result IN 
                SELECT
                    tsvector as tsvector, 
                    ts_rank(query_weights, tsvector, generated_query, query_normalization) as query_rank,
                    txt as txt,
                    document as document
                FROM CHUNK
                WHERE generated_query @@ tsvector AND lang=ts_lang
                ORDER BY query_rank DESC
                LIMIT query_limit
                OFFSET query_offset
            LOOP
                RETURN QUERY SELECT D.document_path, D.description, ts_headline(lang, prelim_result.txt, generated_query, 'MaxFragments=10,FragmentDelimiter="$STA_BR_STA$",StartSel="$STA_BH_STA$",StopSel="$STA_EH_STA$"') FROM DOCUMENT D WHERE D.document_path = prelim_result.document;
            END LOOP;
        END;
    $$
LANGUAGE PLPGSQL;

-- Next, initialize a default instructionary document
SELECT STA_MAKE_DOCUMENT('english', 'SimpleTextArchive/README', 'Read this document on first run, and if desired, remove after', 
$$Hello!

Congratulations! This file shows that you have successfully installed SimpleTextArchive, which is now ready to serve you.

You can add documents using the <simple-text-archive> executable, using command "cmd ingest" with appropriate parameters. You can also use mass ingestion with "bulk-cmd <file with |-separated commands>".
For further help, please see the documentation included.

Cheers!
$$);