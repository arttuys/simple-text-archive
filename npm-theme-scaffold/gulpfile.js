"use strict";
var postcss = require('gulp-postcss');
var concatCss = require('gulp-concat-css');

var csso = require('gulp-csso');
let cleanCSS = require('gulp-clean-css');

var gulp = require('gulp');
var del = require('del');

var sass = require('gulp-sass');
 
sass.compiler = require('node-sass');

/**
 * Declare CSS conversion functions
 */

function internalCss() {
    return gulp.src('assets/scss/*.root.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([ require('precss'), require('autoprefixer'), require('cssnano') ]))
	    .pipe(csso())
        .pipe(concatCss("site.css"))
        .pipe(cleanCSS({level: 2}))	
        .pipe(gulp.dest('./output/'));
};

function externalCss() {
    return gulp.src('assets/external/*.css')
        .pipe(postcss([ require('autoprefixer'), require('cssnano') ]))
	    .pipe(csso())
        .pipe(concatCss("vendor.css"))
        .pipe(cleanCSS({level: 2}))	
        .pipe(gulp.dest('./output/'));
};

function cleanOutput() {
    return del([
      'output/**/*',
    ]);
}

exports.css = gulp.parallel(internalCss, externalCss);
exports.build = gulp.series(cleanOutput, exports.css);