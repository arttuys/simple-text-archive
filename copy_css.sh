#!/bin/sh
set -e
(cd npm-theme-scaffold; npx gulp build)
cp npm-theme-scaffold/output/* ./http_root/static/